//configuration backend express js 

// intialisation
const express= require("express");
const mysql= require("mysql");
const cors= require("cors");
const app= express();
let data= [];

const PORT = process.env.PORT || 4000;
app.use(cors());
app.listen(PORT, err=> {
    if (err) console.log(err)
});
//connect db
const mysql_connection= require("./DataBaseConnection");

// routes to fetch data
app.get('/addInfo', (req,res)=> {
    const {gender, age}= req.query;
    const ADD_INFO_QUERY= `INSERT INTO information (gender, age) VALUES ("${gender}", "${age}")`;
    mysql_connection.query(ADD_INFO_QUERY, async err=> {
        if (err) console.log(err);
        else await res.send('Info Added');
    });
});
app.get('/getData', (req,res)=> {
    const GET_DATA_QUERY= 'SELECT * FROM information';
    mysql_connection.query(GET_DATA_QUERY, async (err, results)=> {
        if (err) console.log(err);
        else await res.json({data:[...results]})
    });
});
app.get('/deleteInfo', (req,res)=> {
    const {Name} = req.query;
    const DELETE_QUERY=`TRUNCATE TABLE information`;
    mysql_connection.query(DELETE_QUERY, err=> {
        if (err) console.log(err);
        else res.send('Info deleted successfully');
    })
})
