import React, { Component } from 'react';
import "bootstrap/dist/css/bootstrap.min.css"
import NavBar from './components/layouts/NavBar';
import Default from './components/pages/Default';
import Register from './components/pages/Register';
import Statistic from './components/pages/Statistic';
import Home from './components/pages/Home';

import {Switch, Route} from 'react-router-dom';
export default class App extends Component {
  render() {
    return (
      <React.Fragment>
         <NavBar></NavBar>
         <Switch>
           <Route exact path='/' component={Home} />
           <Route path='/Register' component={Register} />
           <Route path='/Statistic' component={Statistic} />
           <Route component={Default} />
         </Switch>
      </React.Fragment>
    )
  }
}
