import styled from "styled-components";
 export const ButtonContainer = styled.button `
    text-transform:capitalize;
    text-transform: uppercase;
    font-size:1.4rem;
    background: transparent;
    border: 0.05rem solid var(--lightBlue);
    color: var(--lightBlue);
    border-radius:0.5rem;
    padding-left: 0.5rem;
    padding-right: 0.5rem;
    cursor:pointer;
    margin: 0.2rem 0.5rem 0.2rem 0;
    transition: all 0.5s easin-in-out;
    &:hover {
            background: var(--lightBlue);
            color: var(--mainBlue);
    }
    &:focus{
        outline: none;
    }
`