import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import '../../App.css'
// import styled from 'styled-components'
import {ButtonContainer} from '../buttonstyled/ButtonStyled'
export default class NavBar extends Component {
    render() {
        return (
            <nav className='navbar navbar-expand-sm bg-primary navbar-dark px-sm-5'>
                    <Link to="/">
                        <span className='round'> <i className="fas fa-home home-icon" style={{fontSize:'2.5rem'}}/></span>
                    </Link>
                <ul className='navbar-nav align-items-center ml-auto mr-4'>
                    <li className='nav-item'>
                        <Link to='/Register' >
                            <ButtonContainer>Register</ButtonContainer>
                        </Link>
                    </li>
                </ul>
                <Link to='/Statistic' >
                            <ButtonContainer className='ml-auto'>Statistic</ButtonContainer>
                        </Link>
                
            </nav>
        )
    }
}

