import React, { Component } from 'react'

export default class TableContent extends Component {
    render() {
        const {ID,AGE,GENDER, REGISTER}= this.props.item;
        return (
            <>
              <tr> 
                    <td>{ID}</td>
                    <td> {GENDER}</td>
                    <td>{AGE}</td>
                    <td>{REGISTER}</td>

                </tr>  
            </>
        )
    }
}
