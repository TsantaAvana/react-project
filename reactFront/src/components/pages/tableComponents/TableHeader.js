import React, { Component } from 'react'
import TableContent from './TableContent'
import { ButtonContainer } from '../../buttonstyled/ButtonStyled';
export default class tableheader extends Component {
    componentDidMount() {
        console.log(this.props.data);
    }
    render() {
        const {data}= this.props;
        let data_render= data.map(items=> <TableContent key={items.ID} item={items}/>)
        return (
            <div className='table-container'>
                <div> 
                    <table className='table'>
                        <thead>
                            <tr>
                                <td>ID </td>
                                <td> GENDER </td>
                                <td>AGE </td>
                                <td> REGISTER AT </td>
                            </tr>
                        </thead>
                        <tbody>
                        {data_render} 
                        </tbody>
                    </table>
                </div>
                <div className='footer-table'>
                    <ButtonContainer 
                        className='delete-btn' 
                        onClick={()=>this.props.deleteInfo()}> DELETE TABLES 
                    </ButtonContainer>
                </div>
                
            </ div>    
        )
    }
}
