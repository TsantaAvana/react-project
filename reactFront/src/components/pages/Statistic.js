import React, { Component } from 'react'
import TableHeader from './tableComponents/TableHeader'

export default class Statistic extends Component {
    constructor(props) {
        super(props);
        this.state= {
            data_info:[]
        }
    }
    componentDidMount() {
        this.updateDataInfo();
    }
    updateDataInfo=()=> {
        fetch("http://localhost:4000/getData")
        .then(res=> res.json())
        .then(res=> {
            if(res.data) {
              this.setState({data_info: res.data})
            }
          })
        .catch(err=> console.log(err));
    }
    deleteInfo=()=> {
        fetch("http://localhost:4000/deleteInfo")
    }
    render() {
        if (this.state.data_info.length<=0) {
            return (
                <div className='home plan'>
                    <h1>LOADING...</h1>
                </div>)
        }
        else {
            return (
                <div className='home plan'>
                    <TableHeader 
                        data={this.state.data_info} 
                        deleteInfo={this.deleteInfo}
                    />
                </div>
            )
        }

    }
}
