import React, { Component } from 'react'
import RegisterForm from './registerform/RegisterForm';
export default class Register extends Component {
    constructor() {
        super();
        this.state= {
            gender:'MALE',
            age:0
        }
    }
    handleFormChange= (event) => {
        const {name,value}= event.target;
            this.setState({[name]:value});
    }
    addUserInformation=()=> {
        const {gender, age}= this.state;
        fetch(`http://localhost:4000/addInfo?gender=${gender}&age=${age}`)
            .then(()=> console.log('success'))
            .catch(err=> console.error(err));
    }

    render() {
        return (
            <div className='home plan'>
                <RegisterForm data= {{...this.state}} 
                        handleFormChange={this.handleFormChange} 
                        addUserInformation={this.addUserInformation}
                />   
            </div>
        )
    }
}
