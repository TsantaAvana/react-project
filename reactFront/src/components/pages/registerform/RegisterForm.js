import React, { Component } from 'react'
import {ButtonContainer} from '../../buttonstyled/ButtonStyled'
export default class RegisterForm extends Component {
    render() {
        let event_pass={};
        return (
            <div className='main'>
                   <div className='regform'>  
                        <h1> Registration Form </h1> 
                    </div> 
                    
                    <form className='form-type'>
                        <div id='name'>
                            <label className='subtitle '>GENDER: </label>
                                <select name='gender' className='inbox select-form' 
                                        onChange={e=> {event_pass=e;
                                        this.props.handleFormChange(event_pass)
                                        }}>
                                    <option value="MALE">MALE</option>
                                    <option value="FEMALE">FEMALE</option>
                                    
                                </select>
                            <br />
                            <label className='subtitle '>AGE:</label>
                                <input 
                                    className='lastname inbox' 
                                    type='text' name='age'
                                    onChange={e=> {event_pass=e;
                                        this.props.handleFormChange(event_pass)
                                        }}
                                /> 
                            <br />
                        </div>
                        <div className='footer-form'>
                                    <ButtonContainer 
                                        className='save-btn' 
                                        onClick={(e)=> { e.preventDefault();
                                        this.props.addUserInformation();
                                        alert('Information added')}}>Save
                                    </ButtonContainer>
                        </div>   
                    </form>
                    
                </div>
        )
    }
}
